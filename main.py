import requests


def get_poem_lines(name):
    response = requests.get(f'https://poetrydb.org/title/{name}/lines.json')
    return ''.join([line for line in response.json()[0]['lines']])
